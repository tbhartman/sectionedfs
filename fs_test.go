package sectionedfs_test

import (
	"log"
	"os"
	"testing"
	"time"

	"gitlab.com/tbhartman/sectionedfs"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// new makes a sectioned fs with:
//  - /a/
//  - /a/a1/
//  - /a/a1/a1.txt
//  - /a/a2/
//  - /a/a2/a2.txt
//  - /b/
//  - /b/b.txt
func new() (fs sectionedfs.Fs) {
	a := afero.NewMemMapFs()
	b := afero.NewMemMapFs()
	a.Mkdir("a1", 0700)
	a.Mkdir("a2", 0700)
	b.Mkdir("b1", 0700)
	fa, err := a.Create("a1/a1.txt")
	if err != nil {
		log.Panic(err)
	}
	fa.WriteString("a1.txt")
	fa.Close()
	fa, err = a.Create("a2/a2.txt")
	if err != nil {
		log.Panic(err)
	}
	fa.WriteString("a2.txt")
	fa.Close()
	fb, err := b.Create("b.txt")
	if err != nil {
		log.Panic(err)
	}
	fb.WriteString("b.txt")
	fb.Close()
	fs = sectionedfs.New()
	fs.RegisterSection("a", a)
	fs.RegisterSection("b", b)

	return
}

func CheckFileExists(t *testing.T, fs afero.Fs, path string, size int64) bool {
	s, err := fs.Stat(path)
	if !assert.NoError(t, err) {
		return false
	}
	assert.Equal(t, size, s.Size())
	return assert.False(t, s.IsDir())
}
func CheckNotExists(t *testing.T, fs afero.Fs, path string) bool {
	_, err := fs.Stat(path)
	assert.True(t, os.IsNotExist(err))
	return assert.Error(t, err)
}
func CheckDirExists(t *testing.T, fs afero.Fs, path string) bool {
	s, err := fs.Stat(path)
	if !assert.NoError(t, err) {
		return false
	}
	return assert.True(t, s.IsDir())
}

func TestStat(t *testing.T) {
	fs := new()

	CheckDirExists(t, fs, ".")
	CheckDirExists(t, fs, "a")
	CheckDirExists(t, fs, "a/a1")
	CheckDirExists(t, fs, "a/a2")
	CheckDirExists(t, fs, "b")
	CheckDirExists(t, fs, "b/b1")

	CheckFileExists(t, fs, "a/a1/a1.txt", 6)
	CheckFileExists(t, fs, "a/a2/a2.txt", 6)
	CheckFileExists(t, fs, "b/b.txt", 5)

	var count int

	afero.Walk(fs, ".", func(path string, info os.FileInfo, err error) error {
		count++
		return nil
	})

	assert.EqualValues(t, 9, count)
}

func TestSectionNotExist(t *testing.T) {
	fs := new()
	newfs := afero.NewMemMapFs()
	fs.RegisterSection("c", afero.NewBasePathFs(newfs, "doesnotexist"))
	_, err := fs.Stat("c")
	require.Error(t, err)
	require.True(t, os.IsNotExist(err))
}

func TestRootOpen(t *testing.T) {
	a := assert.New(t)
	r := require.New(t)
	fs := new()
	f, err := fs.Open(".")
	r.NoError(err)
	defer f.Close()
	names, err := f.Readdirnames(0)
	r.NoError(err)
	a.Equal([]string{"a", "b"}, names)
}

func TestChown(t *testing.T) {
	fs := new()

	err := fs.Chown(".", 0, 0)
	assert.Error(t, err)

	err = fs.Chown("a", 0, 0)
	assert.NoError(t, err)

	err = fs.Chown("a/a1", 0, 0)
	assert.NoError(t, err)
}

func TestCreate(t *testing.T) {
	fs := new()

	f, err := fs.Create("c")
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))
	assert.Nil(t, f)

	f, err = fs.Create("a/my.txt")
	assert.NoError(t, err)
	assert.NotNil(t, f)
	f.WriteString("test")
	f.Close()

	CheckFileExists(t, fs, "a/my.txt", 4)
}

func TestRegisterAndRemove(t *testing.T) {
	fs := new()

	a := afero.NewMemMapFs()
	err := fs.RegisterSection("a", a)
	assert.Error(t, err)

	err = fs.RegisterSection("c", a)
	assert.NoError(t, err)

	CheckDirExists(t, fs, "c")
}

func TestRootRemove(t *testing.T) {
	fs := new()

	err := fs.Remove(".")
	assert.True(t, os.IsPermission(err))

	err = fs.Remove("c")
	assert.True(t, os.IsNotExist(err))

	oldAStat, _ := fs.Stat("a")
	oldA, ok := oldAStat.Sys().(afero.Fs)
	assert.True(t, ok)

	err = fs.Remove("a")
	assert.NoError(t, err)

	_, err = fs.Stat("a")
	assert.True(t, os.IsNotExist(err))

	_, err = oldA.Stat("a1")
	assert.NoError(t, err)
}

func TestRootRemoveAll(t *testing.T) {
	fs := new()

	err := fs.RemoveAll(".")
	assert.True(t, os.IsPermission(err))

	err = fs.RemoveAll("c")
	assert.True(t, os.IsNotExist(err))

	oldAStat, _ := fs.Stat("a")
	oldA, ok := oldAStat.Sys().(afero.Fs)
	assert.True(t, ok)

	fs.RemoveAll("a")
	_, err = fs.Stat("a")
	assert.True(t, os.IsNotExist(err))

	CheckDirExists(t, oldA, "a1")
}

func TestRootFile(t *testing.T) {
	fs := new()

	f, err := fs.Open(".")
	require.NoError(t, err)

	assert.Equal(t, ".", f.Name())
	_, err = f.Read([]byte{})
	assert.Error(t, err)

	err = f.Truncate(0)
	assert.Error(t, err)

	err = f.Sync()
	assert.Error(t, err)

	_, err = f.ReadAt([]byte{}, 0)
	assert.Error(t, err)

	_, err = f.Seek(0, 0)
	assert.Error(t, err)

	_, err = f.Write([]byte{})
	assert.Error(t, err)

	_, err = f.WriteAt([]byte{}, 0)
	assert.Error(t, err)

	_, err = f.WriteString("")
	assert.Error(t, err)
}

func TestRemove(t *testing.T) {
	fs := new()

	oldAStat, _ := fs.Stat("a")
	oldA, ok := oldAStat.Sys().(afero.Fs)
	assert.True(t, ok)

	// memmapfs appears to remove even if not empty
	err := fs.Remove("a/a1")
	require.NoError(t, err)

	err = fs.RemoveAll("a/a2")
	assert.NoError(t, err)

	_, err = oldA.Stat("a1")
	assert.True(t, os.IsNotExist(err))
}

func TestMkdir(t *testing.T) {
	fs := new()

	err := fs.Mkdir("c", os.ModeDir)
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	err = fs.MkdirAll("c", os.ModeDir)
	assert.Error(t, err)
	assert.True(t, os.IsPermission(err))

	err = fs.Mkdir("a/a3", os.ModeDir)
	assert.NoError(t, err)

	err = fs.MkdirAll("a/a3/a3/a3", os.ModeDir)
	assert.NoError(t, err)

	CheckDirExists(t, fs, "a/a3/a3/a3")
}

func TestRootReadPart(t *testing.T) {
	a := assert.New(t)
	r := require.New(t)
	fs := new()
	f, _ := fs.Open(".")
	defer f.Close()
	names, err := f.Readdirnames(1)
	r.NoError(err)
	a.Equal(1, len(names))
	a.Equal("a", names[0])
	names, err = f.Readdirnames(1)
	r.NoError(err)
	a.Equal(1, len(names))
	a.Equal("b", names[0])
	names, err = f.Readdirnames(1)
	r.Error(err)
	a.Nil(names)
}

func TestRootStat(t *testing.T) {
	a := assert.New(t)
	r := require.New(t)

	start := time.Now()
	fs := new()
	end := time.Now()

	s, err := fs.Stat(".")
	r.NoError(err)
	r.NotNil(s)

	a.True(s.IsDir())
	a.True(s.ModTime().Before(end))
	a.True(s.ModTime().After(start))
	a.Equal(os.ModeDir|0700, s.Mode())
	a.Equal(fs, s.Sys())
	a.EqualValues(0, s.Size())

	f, err := fs.Open(".")
	r.NoError(err)
	r.NotNil(f)
	defer f.Close()
	s, err = f.Stat()
	r.NoError(err)
	r.NotNil(s)
	a.Equal(fs, s.Sys())
}

func TestOpenEmptySection(t *testing.T) {
	fs := new()

	_, err := fs.Open("c")
	assert.Error(t, err)
	assert.True(t, os.IsNotExist(err))
}

func TestRenameSection(t *testing.T) {
	fs := new()

	s, _ := fs.Stat("a/a1/a1.txt")
	require.NotNil(t, s)
	assert.False(t, s.IsDir())

	err := fs.Rename("c", "b")
	assert.Error(t, err)
	err = fs.Rename("a", "b")
	assert.Error(t, err)
	err = fs.Rename("a", "c")
	assert.NoError(t, err)

	d, _ := fs.Open(".")
	names, _ := d.Readdirnames(0)
	assert.EqualValues(t, []string{"b", "c"}, names)

	CheckFileExists(t, fs, "c/a1/a1.txt", 6)
}

func TestCloseTwice(t *testing.T) {
	fs := new()
	f, err := fs.Open(".")
	require.NoError(t, err)
	err = f.Close()
	assert.NoError(t, err)
}

func TestSectionFs(t *testing.T) {
	a := assert.New(t)
	memFs := afero.NewMemMapFs()
	memFs.Mkdir("A", os.ModeDir)
	f, _ := memFs.Create("A/a.txt")
	f.Close()
	memFs.Mkdir("B", os.ModeDir)
	f, _ = memFs.Create("B/b.txt")
	f.Close()
	ba := afero.NewBasePathFs(memFs, "A")
	bb := afero.NewBasePathFs(memFs, "B")
	secFs := sectionedfs.New()
	secFs.RegisterSection("DirA", ba)
	secFs.RegisterSection("DirB", bb)

	CheckFileExists(t, secFs, "DirA/a.txt", 0)
	CheckFileExists(t, secFs, "DirB/b.txt", 0)
	_, err := secFs.Stat("A/a.txt")
	a.Error(err)
}

func TestRename(t *testing.T) {
	t.Run("FileToFile", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "a/a2/a2.txt")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "a/a2/a2.txt", 6)
	})
	t.Run("FileToFileAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "b/b.txt")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "b/b.txt", 6)
	})
	t.Run("FileToInvalid", func(t *testing.T) {
		fs := new()
		// memmapfs does this even though it shouldn't
		err := fs.Rename("a/a1/a1.txt", "a/a3/a1.txt")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "a/a3/a1.txt", 6)
	})
	t.Run("FileToInvalidAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "b/another/b.txt")
		assert.Error(t, err)

		CheckFileExists(t, fs, "a/a1/a1.txt", 6)
		CheckNotExists(t, fs, "b/another")
	})
	t.Run("FileToNewFile", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "a/a1/a2.txt")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "a/a1/a2.txt", 6)
	})
	t.Run("FileToNewFileAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "b/a1.txt")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "b/a1.txt", 6)
	})
	t.Run("FileToDir", func(t *testing.T) {
		fs := new()
		// memmapfs does not handle this correctly
		err := fs.Rename("a/a1/a1.txt", "a/a2")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "a/a2", 6)
	})
	t.Run("FileToDirAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1/a1.txt", "b")
		assert.NoError(t, err)

		CheckNotExists(t, fs, "a/a1/a1.txt")
		CheckFileExists(t, fs, "b/a1.txt", 6)
	})
	t.Run("DirToFile", func(t *testing.T) {
		fs := new()
		// memmapfs renames this even though it is a file
		fs.Rename("a/a2", "a/a1/a1.txt")
		CheckDirExists(t, fs, "a/a1/a1.txt")
		CheckNotExists(t, fs, "a/a2")
	})
	t.Run("DirToFileAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a2", "b/b.txt")
		assert.Error(t, err)
		CheckDirExists(t, fs, "a/a2")
		CheckFileExists(t, fs, "b/b.txt", 5)
	})
	t.Run("DirToInvalid", func(t *testing.T) {
		fs := new()
		// memmapfs renames this even though the parent doesn't exist
		err := fs.Rename("a/a1", "a/a3/a1")
		assert.NoError(t, err)
		CheckNotExists(t, fs, "a/a1")
		CheckDirExists(t, fs, "a/a3")
	})
	t.Run("DirToInvalidAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1", "b/a3/a1")
		assert.Error(t, err)
		CheckDirExists(t, fs, "a/a1")
		CheckNotExists(t, fs, "b/a3")
	})

	t.Run("DirToDirAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1", "b")
		assert.NoError(t, err)
		CheckNotExists(t, fs, "a/a1")
		CheckFileExists(t, fs, "b/a1/a1.txt", 6)
	})
	t.Run("DirToNewDirAcrossFs", func(t *testing.T) {
		fs := new()
		err := fs.Rename("a/a1", "b/a3")
		assert.NoError(t, err)
		CheckNotExists(t, fs, "a/a1")
		CheckFileExists(t, fs, "b/a3/a1.txt", 6)
	})
	t.Run("DirToNonEmptyDirAcrossFs", func(t *testing.T) {
		fs := new()
		fs.MkdirAll("b/directory/a1", os.ModeDir|0700)
		f, _ := fs.Create("b/directory/a1/file")
		f.WriteString("file")
		f.Close()

		err := fs.Rename("a/a1", "b/directory")
		assert.Error(t, err)

		CheckDirExists(t, fs, "a/a1")
		CheckFileExists(t, fs, "b/directory/a1/file", 4)
		CheckNotExists(t, fs, "b/directory/a1/a1.txt")
	})
}

func TestChmod(t *testing.T) {
	fs := new()

	err := fs.Chmod(".", os.ModeDir|0700)
	assert.Error(t, err)

	err = fs.Chmod("a", os.ModeDir|0700)
	assert.Error(t, err)

	err = fs.Chmod("a/a1/a1.txt", 0700)
	assert.NoError(t, err)

	err = fs.Chmod("c", os.ModeDir)
	assert.True(t, os.IsNotExist(err))
}

func TestChtimes(t *testing.T) {
	fs := new()

	err := fs.Chtimes(".", time.Now(), time.Now())
	assert.Error(t, err)

	err = fs.Chtimes("a", time.Now(), time.Now())
	assert.Error(t, err)

	err = fs.Chtimes("a/a1/a1.txt", time.Now(), time.Now())
	assert.NoError(t, err)

	err = fs.Chtimes("c", time.Now(), time.Now())
	assert.True(t, os.IsNotExist(err))
}
