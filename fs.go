// Package sectionedfs implements an afero.Fs filesystem in which
// immediate subdirectories are named by strings mapped to other
// afero.Fs filesystems.  You can combine disparate directories
// from multiple filesystems into what is accessed as a single
// directory.
package sectionedfs

import (
	"errors"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/spf13/afero"
)

// Fs implements the afero.Fs interface for a group of
// named afero.Fs's.  Each sub-Fs is accessed via the
// first element of the paths given to Fs
type Fs interface {
	afero.Fs
	RegisterSection(name string, sec afero.Fs) error
}

type sectionedFs struct {
	sections map[string]afero.Fs
	modTime  time.Time
}

func (s *sectionedFs) splitPath(path string) (section afero.Fs, sectionName string, subpath string) {
	path = filepath.Clean(path)
	path = strings.TrimPrefix(path, string(filepath.Separator))
	split := strings.SplitN(path, string(filepath.Separator), 2)
	if len(split) > 0 {
		sectionName = split[0]
		section, _ = s.sections[sectionName]
	}
	if len(split) > 1 {
		subpath = split[1]
	}
	return
}

// Create implements the afero.fs.Create interface.
// Attempting to Create under the root directory returns an os.ErrPermission.
func (s *sectionedFs) Create(name string) (afero.File, error) {
	sec, _, subpath := s.splitPath(name)
	if sec == nil {
		return nil, os.ErrPermission
	}
	return sec.Create(subpath)
}

// Chown implements the afero.fs.Chown interface.
// Attempting to Chown the root directory returns an os.ErrPermission.
func (s *sectionedFs) Chown(name string, uid, gid int) error {
	sec, _, subpath := s.splitPath(name)
	if sec == nil {
		return os.ErrPermission
	}
	return sec.Chown(subpath, uid, gid)
}

// Mkdir implements the afero.fs.Mkdir interface.
// Attempting to create a directory at the root level returns an
// os.ErrPermission.
func (s *sectionedFs) Mkdir(name string, perm os.FileMode) error {
	sec, _, subpath := s.splitPath(name)
	if sec == nil {
		return os.ErrPermission
	}
	return sec.Mkdir(subpath, perm)
}

// MkdirAll implements the afero.fs.MkdirAll interface.
// Attempting to create a directory at the root level (or under a section that
// does not exist) returns an os.ErrPermission.
func (s *sectionedFs) MkdirAll(path string, perm os.FileMode) error {
	sec, _, subpath := s.splitPath(path)
	if sec == nil {
		return os.ErrPermission
	}
	return sec.MkdirAll(subpath, perm)
}

// Name implements the afero.fs.Name interface.
func (s *sectionedFs) Name() string {
	return "SectionedFs"
}

// Open implements the afero.fs.Open interface.
func (s *sectionedFs) Open(name string) (afero.File, error) {
	return s.OpenFile(name, os.O_RDONLY, 0)
}

// OpenFile implements the afero.fs.OpenFile interface.
func (s *sectionedFs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {
	sec, secname, subpath := s.splitPath(name)
	switch {
	case sec == nil && secname == ".":
		// attempt to open this
		return &sectionedFsDir{stat: s.getRootInfo(), infos: s.getInfos(), name: secname}, nil
	case sec == nil:
		return nil, os.ErrNotExist
	default:
		return sec.OpenFile(subpath, flag, perm)
	}
}

// Remove implements the afero.fs.Remove interface.  You may remove
// a section named `a` via `Remove("a")`.  This does not affect any
// data in the underlying afero.Fs.
func (s *sectionedFs) Remove(name string) error {
	sec, secname, subpath := s.splitPath(name)
	if secname == "." {
		return os.ErrPermission
	}
	if sec == nil {
		return os.ErrNotExist
	}
	if subpath == "" {
		delete(s.sections, secname)
		return nil
	}
	return sec.Remove(subpath)
}

// RemoveAll implements the afero.fs.RemoveAll interface.  You may remove
// a section named `a` via `RemoveAll("a")`.  This does not affect any
// data in the underlying afero.Fs.
func (s *sectionedFs) RemoveAll(path string) error {
	sec, secname, subpath := s.splitPath(path)
	if secname == "." {
		return os.ErrPermission
	}
	if sec == nil {
		return os.ErrNotExist
	}
	if subpath == "" {
		delete(s.sections, secname)
		return nil
	}
	return sec.RemoveAll(subpath)
}

// Stat implements the afero.fs.Stat interface.
func (s *sectionedFs) Stat(name string) (os.FileInfo, error) {
	sec, secname, subpath := s.splitPath(name)
	if secname == "." {
		return s.getRootInfo(), nil
	}
	if sec == nil {
		return nil, os.ErrNotExist
	}
	if subpath == "" {
		return s.getSectionInfo(secname)
	}
	return sec.Stat(subpath)
}

// Chmod implements the afero.fs.Chmod interface.
func (s *sectionedFs) Chmod(name string, mode os.FileMode) error {
	sec, secname, subpath := s.splitPath(name)
	if secname == "." {
		return os.ErrPermission
	}
	if sec == nil {
		return os.ErrNotExist
	}
	return sec.Chmod(subpath, mode)
}

// Chtimes implements the afero.fs.Chtimes interface.
func (s *sectionedFs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	sec, secname, subpath := s.splitPath(name)
	if secname == "." || (sec != nil && subpath == "") {
		return os.ErrPermission
	}
	if sec == nil {
		return os.ErrNotExist
	}
	return sec.Chtimes(subpath, atime, mtime)
}
func (s *sectionedFs) RegisterSection(name string, sec afero.Fs) error {
	_, ok := s.sections[name]
	if ok {
		return errors.New("Section already exists")
	}
	s.sections[name] = sec
	s.modTime = time.Now()
	return nil
}
func (s *sectionedFs) getInfos() (ret []os.FileInfo) {
	var names []string
	for k := range s.sections {
		names = append(names, k)
	}
	sort.Strings(names)

	for _, name := range names {
		info, err := s.getSectionInfo(name)
		if err == nil {
			ret = append(ret, info)
		}
	}
	return
}
func (s *sectionedFs) getSectionInfo(name string) (os.FileInfo, error) {
	value := s.sections[name]
	info, _ := value.Stat(".")
	if info == nil {
		return nil, os.ErrNotExist
	}
	return &genericStat{name: name, modTime: info.ModTime(), fs: value}, nil
}

func (s *sectionedFs) getRootInfo() (ret os.FileInfo) {
	return &genericStat{name: ".", modTime: s.modTime, fs: s}
}

// New creates a new SectionedFs
func New() Fs {
	return &sectionedFs{sections: make(map[string]afero.Fs, 0)}
}
