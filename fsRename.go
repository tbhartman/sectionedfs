package sectionedfs

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/spf13/afero"
)

// Rename implements the afero.fs.Rename interface.  Renaming files across
// section boundaries (across the contained afero.Fs's) is supported and
// results in a copy operation.
func (s *sectionedFs) Rename(oldname, newname string) error {
	oldSec, oldKey, subpath := s.splitPath(oldname)
	newSec, newKey, subNewPath := s.splitPath(newname)
	switch {
	// renaming a whole section
	case oldSec != nil && newSec == nil && newKey != "." && subpath == "" && subNewPath == "":
		delete(s.sections, oldKey)
		s.sections[newKey] = oldSec
		return nil
	// old or new doesn't exist
	case oldSec == nil || newSec == nil:
		return os.ErrNotExist
	// attempting to move entire section
	case subpath == "":
		return os.ErrPermission
	// rename occurs within one fs
	case oldKey == newKey:
		return oldSec.Rename(subpath, subNewPath)
	}

	// attempting to move from within oldsec to within newsec
	var oldIsRWable bool = true
	afero.Walk(oldSec, subpath, func(path string, info os.FileInfo, err error) error {
		if info.Mode()|0600 == 0 {
			oldIsRWable = false
			return os.ErrInvalid
		}
		return nil
	})
	if !oldIsRWable {
		return os.ErrPermission
	}
	oldStat, err := s.Stat(oldname)
	if err != nil {
		return err
	}
	newStat, err := s.Stat(newname)

	switch {
	// file to file
	case err == nil && !oldStat.IsDir() && !newStat.IsDir():
		if newStat.Mode()|0600 == 0 {
			return os.ErrPermission
		}
		oldf, _ := s.Open(oldname)
		newf, _ := s.Create(newname)
		_, err := io.Copy(newf, oldf)
		newf.Close()
		oldf.Close()
		if err != nil {
			return err
		}
		s.Remove(oldname)
		return nil
	// file to new path
	case os.IsNotExist(err) && !oldStat.IsDir():
		// parent must exist
		_, newParErr := s.Stat(filepath.Dir(newname))
		if newParErr != nil {
			return newParErr
		}
		newf, err := s.Create(newname)
		if err != nil {
			return err
		}
		oldf, _ := s.Open(oldname)
		io.Copy(newf, oldf)
		newf.Close()
		oldf.Close()
		s.Remove(oldname)
		return nil
	// dir to new path
	case os.IsNotExist(err) && oldStat.IsDir():
		_, parderErr := s.Stat(filepath.Dir(newname))
		if parderErr != nil {
			return parderErr
		}
		// create the new directory and copy files over, then delete old path
		// err := s.Mkdir(newname, oldStat.Mode())
		// if err != nil {
		// 	return err
		// }
		err := afero.Walk(s, oldname, func(path string, info os.FileInfo, err error) error {
			rel, err := filepath.Rel(oldname, path)
			newPath := filepath.Join(newname, rel)
			if err != nil {
				return err
			}
			if path != "." {
				if info.IsDir() {
					s.Mkdir(newPath, info.Mode())
				} else {
					oldF, err := s.Open(path)
					if err != nil {
						return err
					}
					newF, err := s.Create(newPath)
					if err != nil {
						return err
					}
					_, err = io.Copy(newF, oldF)
					oldF.Close()
					newF.Close()
					if err != nil {
						return err
					}
				}
			}
			return nil
		})
		if err == nil {
			err = s.RemoveAll(oldname)
		}
		return err
	// dir to file
	case err == nil && oldStat.IsDir() && !newStat.IsDir():
		return &os.PathError{Op: "rename", Path: newname, Err: fmt.Errorf("overwriting file with directory")}
	// dir to dir:
	case err == nil && oldStat.IsDir() && newStat.IsDir():
		// possibilities:
		//   - old base name does not exist in new directory: rename to newname/oldbasename
		//   - old base name exists in new directory and is empty: remove the empty directory and rename to newname/oldbasename
		//   - old base name exists in new directory and is not empty: do not allow
		newpath := filepath.Join(newname, filepath.Base(oldname))
		newdir, err := s.Open(newpath)
		if os.IsNotExist(err) {
			return s.Rename(oldname, newpath)
		}
		if err != nil {
			return err
		}
		names, err := newdir.Readdirnames(0)
		if err != nil {
			return err
		}
		if len(names) != 0 {
			return &os.PathError{Op: "rename", Path: newpath, Err: fmt.Errorf("directory not empty")}
		}
		// attempting to replace directory
		err = os.Remove(newpath)
		if err != nil {
			return err
		}
		return s.Rename(oldname, newpath)
	// file to dir
	case err == nil && !oldStat.IsDir() && newStat.IsDir():
		return s.Rename(oldname, filepath.Join(newname, filepath.Base(oldname)))
	}
	panic("unexpected rename operation")
}
