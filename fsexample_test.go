package sectionedfs_test

import (
	"fmt"
	"io/ioutil"

	"github.com/spf13/afero"
	"gitlab.com/tbhartman/sectionedfs"
)

func ExampleFs() {
	a := afero.NewMemMapFs()
	b := afero.NewMemMapFs()

	parentFs := sectionedfs.New()
	parentFs.RegisterSection("a", a)
	parentFs.RegisterSection("b", b)

	fs := parentFs.(afero.Fs)
	fs.MkdirAll("a/dir_under_a", 0700)
	fs.MkdirAll("b/dir_under_b", 0700)

	fa, _ := fs.Create("a/dir_under_a/file_in_a")
	fa.WriteString("sample text\n")
	fa.Close()

	fileViaChild, _ := a.Open("dir_under_a/file_in_a")
	contents, _ := ioutil.ReadAll(fileViaChild)
	fmt.Print(string(contents))
	// Output: sample text
}
