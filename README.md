Sectioned Fs
============

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/tbhartman/sectionedfs.svg)](https://pkg.go.dev/gitlab.com/tbhartman/sectionedfs)
[![pipeline status](https://gitlab.com/tbhartman/sectionedfs/badges/master/pipeline.svg)](https://gitlab.com/tbhartman/sectionedfs/-/commits/master)
[![coverage report](https://gitlab.com/tbhartman/sectionedfs/badges/master/coverage.svg)](https://gitlab.com/tbhartman/sectionedfs/-/commits/master)
[![goreport](https://goreportcard.com/badge/gitlab.com/tbhartman/sectionedfs)](https://goreportcard.com/report/gitlab.com/tbhartman/sectionedfs)

This package implements an [`afero.Fs`](https://pkg.go.dev/github.com/spf13/afero#Fs)
with directories at the root mapped by strings to other `afero.Fs`'s.

The interface adds two

```go
package sectionedfs_test

import (
	"fmt"
	"io/ioutil"

	"github.com/spf13/afero"
	"gitlab.com/tbhartman/sectionedfs"
)

func ExampleFs() {
    a := afero.NewMemMapFs()
    b := afero.NewMemMapFs()

    parentFs := sectionedfs.New()
    parentFs.RegisterSection("a", a)
    parentFs.RegisterSection("b", b)

    fs := parentFs.(afero.Fs)
    fs.MkdirAll("/a/dir_under_a", 0700)
    fs.MkdirAll("/b/dir_under_b", 0700)

    fa, _ := fs.Create("/a/dir_under_a/file_in_a")
    fa.WriteString("sample text\n")
    fa.Close()

    fileViaChild, _ := a.Open("/dir_under_a/file_in_a")
    contents, _ := ioutil.ReadAll(fileViaChild)
    fmt.Print(string(contents))
    // Output: sample text
}
```
