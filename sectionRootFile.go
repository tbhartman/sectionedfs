package sectionedfs

import (
	"io"
	"os"
)

type sectionedFsDir struct {
	stat   os.FileInfo
	name   string
	infos  []os.FileInfo
	pos    int
	closed bool
}

func (s *sectionedFsDir) Name() string { return s.name }

// these methods pertain to files
func (s *sectionedFsDir) Close() error                                 { return nil }
func (s *sectionedFsDir) Read(p []byte) (int, error)                   { return 0, os.ErrInvalid }
func (s *sectionedFsDir) Seek(offset int64, whence int) (int64, error) { return 0, os.ErrInvalid }
func (s *sectionedFsDir) Stat() (os.FileInfo, error)                   { return s.stat, nil }
func (s *sectionedFsDir) Sync() error                                  { return os.ErrInvalid }
func (s *sectionedFsDir) Truncate(size int64) error                    { return os.ErrInvalid }
func (s *sectionedFsDir) Write(b []byte) (int, error)                  { return 0, os.ErrInvalid }
func (s *sectionedFsDir) WriteAt(b []byte, off int64) (int, error)     { return 0, os.ErrInvalid }
func (s *sectionedFsDir) WriteString(str string) (int, error)          { return 0, os.ErrInvalid }
func (s *sectionedFsDir) ReadAt(p []byte, off int64) (int, error)      { return 0, os.ErrInvalid }

func (s *sectionedFsDir) Readdir(n int) (ret []os.FileInfo, err error) {
	if s.pos >= len(s.infos) {
		return nil, io.EOF
	}
	if n > 0 {
		ret = s.infos[s.pos : s.pos+n]
		s.pos += n
		return
	}
	s.pos = len(s.infos)
	return s.infos, nil
}
func (s *sectionedFsDir) Readdirnames(n int) (ret []string, err error) {
	infos, err := s.Readdir(n)
	if err != nil {
		return
	}
	for _, i := range infos {
		ret = append(ret, i.Name())
	}
	return
}
