package sectionedfs

import (
	"os"
	"time"

	"github.com/spf13/afero"
)

type genericStat struct {
	name    string
	modTime time.Time
	fs      afero.Fs
}

func (s *genericStat) IsDir() bool        { return true }
func (s *genericStat) ModTime() time.Time { return s.modTime }
func (s *genericStat) Mode() os.FileMode  { return os.ModeDir | 0700 }
func (s *genericStat) Name() string       { return s.name }
func (s *genericStat) Size() int64        { return 0 }
func (s *genericStat) Sys() interface{}   { return s.fs }
